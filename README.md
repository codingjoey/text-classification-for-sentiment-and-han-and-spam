University of Southern California

CSCI544 Natural Language Processing

Homework1: Text classification





Description:
In this assignment I created the text classifier and apply it to two datasets corresponding to two tasks: (1) spam filtering, and (2) sentiment analysis. In part I you developed a Naive Bayes classifier and applied it to the two datasets. In part II I use off-the-shelf classifiers (SVM Light and MegaM) with the same datasets.


Question 1: What are the precision, recall and F-score on the development data for your classifier in part I for each of the two datasets. Report precision, recall and F-score for each label.

Precision, Recall and F-score:

Part1: (Naive Bayes classifier)

Spam filtering (SPAM):

prescision = 0.9045226130653267

recall = 0.9917355371900827

F-Score = 0.9461235216819974


Sentiment analysis(moview review: positive or negative):

Positive:

prescision = 0.8448117539026629

recall = 0.736

F-Score = 0.7866609662248824

Negative:

prescision = 0.7661233167965982

recall = 0.8648

F-Score = 0.8124765125892521



Question 2: What are the precision, recall and F-score for your classifier in part II for each of the two datasets. Report precision, recall and F-score for each label.

part2:(SVM Light and MegaM)

SVM Light:

Spam filtering (SPAM or HAM):

SPAM:

prescision = 0.9568106312292359

recall = 0.7933884297520661

F-Score = 0.8674698795180723

HAM:

prescision = 0.9293785310734464

recall = 0.987

F-Score = 0.9573229873908826

Sentiment analysis(moview review: positive or negative):

Positive:

prescision = 0.8573141486810552

recall = 0.858

F-Score = 0.8576569372251099

Negative:

prescision = 0.8578863090472378

recall = 0.8572

F-Score = 0.8575430172068828



MegaM:

Spam filtering (SPAM or HAM):

SPAM:

prescision = 0.9751381215469613

recall = 0.9724517906336089

F-Score = 0.9737931034482759

HAM:

prescision = 0.99000999000999

recall = 0.991

F-Score = 0.9905047476261869


Sentiment analysis(moview review: positive or negative):

Positive:

prescision = 0.8633360858794384

recall = 0.8364

F-Score = 0.8496546119463634

Negative:

prescision = 0.8413498836307215

recall = 0.8676

F-Score = 0.854273335959039


Question 3: What happens exactly to precision, recall and F-score in each of the two tasks (on the development data) when only 10% of the training data is used to train the classifiers in part I and part II? Why do you think that is?
If only 10% data is used, the accuracy will be much lower. However, it still depends on how to choose the 10% training data. This will heavily affect the accuracy, too. For example, if this 10% all come from only one class, then the accuracy would be much lower than choose the 10% training data randomly.










How to use (Usage):

Part1:

Spam filtering (SPAM or HAM):

I use training data provided to learn and use development data provided to test.

operations:

1. Generate the training file:

python3 genTrainFileSpam.py training_data_HAM training_data_SPAM spam_training_data

EX: python3 genTrainFileSpam.py '../../SPAM_training/HAM.*.txt' '../../SPAM_training/SPAM.*.txt' '../spam_training.txt'

2. Generate the testing files:

python3 genTestSPAM.py development_file_for_testing test_file verify_file

EX1: python3 genTestSPAM.py '../../SPAM_dev/*.txt' '../testFileSPAM.txt' 'verifyModelSPAM.txt'

EX2: python3 genTestSPAM.py '../../SPAM_test/*.txt' '../testFileSPAM.txt' 'verifyModelSPAM.txt'

Note: Verify_file is used to compare to the actual ourput file to compute f-score.

3. Learning:

python3 nblearn.py train_file model_file

EX: python3 nblearn.py spam_training.txt spam.nb

4. Classify:

python3 nbclassify.py model_file test_file > out_file

EX: python3 nbclassify.py spam.nb testFileSPAM.txt > spam.out

5. Calculate F-Score for SPAM class:(Only when yuo did about with EX1 in step2)

python3 calFScoreSpam.py actual_file out_file

EX: python3 calFScoreSpam.py verifyModelSPAM.txt ../spam.out


Sentiment analysis(moview review: positive or negative):

Here, because there is only training data, I divide this and use 80% of them to be the training data and 20% of them to be the development data (test data).

operations:

1. Generate the training file:

python3 genTrainFileSentiment.py training_data_negative training_data_positive sentiment_training_data

EX1: python3 genTrainFileSentiment.py '../../SENTIMENT_training80/NEG.*.txt' '../../SENTIMENT_training80/POS.*.txt' '../sentiment_training.txt'

EX2: python3 genTrainFileSentiment.py '../../SENTIMENT_training/NEG.*.txt' '../../SENTIMENT_training/POS.*.txt' '../sentiment_training.txt'


2. Generate the testing files:

python3 genTestSENTIMENT.py development_file_for_testing test_file verify_file

EX1: python3 genTestSENTIMENT.py '../../SENTIMENT_dev20/*.txt' '../testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt'

EX2: python3 genTestSENTIMENT.py '../../SENTIMENT_test/*.txt' '../testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt'

Verify_file is used to compare to the actual ourput file to compute f-score.

3. Learning:

python3 nblearn.py train_file model_file

EX: python3 nblearn.py sentiment_training.txt sentiment.nb

4. Classify:

python3 nbclassify.py model_file test_file > out_file

EX: python3 nbclassify.py sentiment.nb testFileSENTIMENT.txt > sentiment.out

5. Calculate F-Score for Negatice  and Positive class: (Only when yuo did about with EX1 in step1&2)

python3 calFScoreSentimentNEG.py actual_file out_file

EX: python3 calFScoreSentimentNEG.py verifyModelSENTIMENT.txt ../sentiment.out

python3 calFScoreSentimentPOS.py actual_file out_file

EX: python3 calFScoreSentimentPOS.py verifyModelSENTIMENT.txt ../sentiment.out



part2:

SVM Light:

Spam filtering (SPAM or HAM):

1. Generate the training file:

python3 genTrainFileSpamSVM.py training_data_HAM training_data_SPAM sentiment_training_data

EX: python3 genTrainFileSpamSVM.py '../../../SPAM_training/HAM.*.txt' '../../../SPAM_training/SPAM.*.txt' 'trainDataSPAM_SVM.txt'

2. Generate the testing files:

python3 genTestFileSPAMSVM.py development_file_for_testing test_file verify_file

EX1: python3 genTestFileSPAMSVM.py '../../../SPAM_dev/*.txt' 'testFileSPAMSVM.txt' 'verifyModelSPAMSVM.txt'

EX2: python3 genTestFileSPAMSVM.py '../../../SPAM_test/*.txt' 'testFileSPAMSVM.txt' 'verifyModelSPAMSVM.txt'

3. Learning:

svm_learn [options] example_file model_file

EX: ./svm_learn trainDataSPAM_SVM.txt ../spam.svm.model

4. Classify:

svm_classify [options] example_file model_file output_file

EX: ./svm_classify testFileSPAMSVM.txt ../spam.svm.model spam.svm.result

5. Postprocess the result to make the output fit the request:

EX: python3 convertResultFileSpamSVM.py

6. Calculate F-Score for SPAM class:(Only when yuo did about with EX1 in step2)

python3 calFScoreSpam.py actual_file out_file

EX1:python3 calFScoreSpamSVM.py verifyModelSPAMSVM.txt ../spam.svm.out

EX1:python3 calFScoreHamSVM.py verifyModelSPAMSVM.txt ../spam.svm.out


Sentiment analysis(moview review: positive or negative):

Here, because there is only training data, I divide this and use 80% of them to be the training data and 20% of them to be the development data (test data).

operations:

1. Generate the training file:

python3 genTrainFileSentimentSVM.py train_file_POS train_file_NEG train_file_name

EX1:  python3 genTrainFileSentimentSVM.py '../../../SENTIMENT_training80/POS.*.txt' '../../../SENTIMENT_training80/NEG.*.txt' 'trainDataSENTIMENT_SVM.txt'

EX2:  python3 genTrainFileSentimentSVM.py '../../../SENTIMENT_training/POS.*.txt' '../../../SENTIMENT_training/NEG.*.txt' 'trainDataSENTIMENT_SVM.txt'

2. Generate the testing files:

Usage:   python3 genTestFileSENTIMENTSVM.py  dev_file test_file verify_file

EX1: python3 genTestFileSENTIMENTSVM.py '../../../SENTIMENT_dev20/*.txt' 'testFileSENTIMENTSVM.txt' 'verifyModelSENTIMENTSVM.txt'

EX2: python3 genTestFileSENTIMENTSVM.py '../../../SENTIMENT_test/*.txt' 'testFileSENTIMENTSVM.txt' 'verifyModelSENTIMENTSVM.txt'

3. Learning:

svm_learn [options] example_file model_file

EX: ./svm_learn trainDataSENTIMENT_SVM.txt ../sentiment.svm.model

4. Classify:

svm_classify [options] example_file model_file output_file

EX: ./svm_classify testFileSENTIMENTSVM.txt ../sentiment.svm.model sentiment.svm.result

5. Postprocess the result to make the output fit the request:

EX: python3 convertResultFileSentimentSVM.py

6. Calculate F-Score for Negatice  and Positive class: (Only when yuo did about with EX1 in step1&2)

Usage: python3 calFScoreSentimentPOSSVM.py VERIFYMODELFILE TESTFILE

EX: python3 calFScoreSentimentPOSSVM.py verifyModelSENTIMENTSVM.txt ../sentiment.svm.out

Usage: python3 calFScoreSentimentPOSSVM.py VERIFYMODELFILE TESTFILE

EX: python3 calFScoreSentimentPOSSVM.py verifyModelSENTIMENTSVM.txt ../sentiment.svm.out



MegaM:

Spam filtering (SPAM or HAM):

1. Generate the training file:

Usage: python3 genTrainFileSpamMega.py train_file_HAM train_file_SPAM train_file

EX:  python3 genTrainFileSpamMega.py '../../../SPAM_training/HAM.*.txt' '../../../SPAM_training/SPAM.*.txt' 'trainDataSPAM_Mega.txt'

2. Generate the testing files:

Usage:   python3 genTestSPAM.py test_file verifyModelFile

EX1: python3 genTestSPAM.py '../../../SPAM_dev/*.txt' 'testFileSPAM.txt' 'verifyModelSPAM.txt'

EX2: python3 genTestSPAM.py '../../../SPAM_test/*.txt' 'testFileSPAM.txt' 'verifyModelSPAM.txt'

3. Learning:

Usage: megam [options] <model-type> <input-file>  (./megam.opt -maxi 10 binary small2 > weights_b)

EX: ./megam_0.92/megam binary trainDataSPAM_Mega.txt > ../spam.megam.model

4. Classify:

Usage: ./megam.opt -predict Model_file binary test_file

EX: ./megam_0.92/megam -predict ../spam.megam.model binary testFileSPAM.txt > spam.megam.result

5. Postprocess the result to make the output fit the request:

EX: python3 convertResultFileMegamSPAM.py

6. Calculate F-Score for SPAM class:(Only when yuo did about with EX1 in step2)

python3 calFScoreSpamMEGAM.py actual_file out_file

EX1:python3 calFScoreSpamMEGAM.py verifyModelSPAM.txt ../spam.megam.out

EX2:python3 calFScoreHamMEGAM.py verifyModelSPAM.txt ../spam.megam.out


Sentiment analysis(moview review: positive or negative):

Here, because there is only training data, I divide this and use 80% of them to be the training data and 20% of them to be the development data (test data).

operations:

1. Generate the training file:

Usage: python3 genTrainFileSentimentMega.py train_file_HAM train_file_SPAM train_file

EX1:  python3 genTrainFileSentimentMega.py '../../../SENTIMENT_training80/POS.*.txt' '../../../SENTIMENT_training80/NEG.*.txt' 'trainDataSENTIMENT_Mega.txt'

EX2:  python3 genTrainFileSentimentMega.py '../../../SENTIMENT_training/POS.*.txt' '../../../SENTIMENT_training/NEG.*.txt' 'trainDataSENTIMENT_Mega.txt'

2. Generate the testing files:

Usage:   python3 genTestSENTIMENT.py test_file verifyModelFile

EX1: python3 genTestSENTIMENT.py '../../../SENTIMENT_dev20/*.txt' 'testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt'

EX2: python3 genTestSENTIMENT.py '../../../SENTIMENT_test/*.txt' 'testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt'

3. Learning:

Usage: megam [options] <model-type> <input-file>  (./megam.opt -maxi 10 binary small2 > weights_b)

EX: ./megam_0.92/megam binary trainDataSENTIMENT_Mega.txt > ../sentiment.megam.model

4. Classify:

Usage: ./megam.opt -predict Model_file binary test_file

EX: ./megam_0.92/megam -predict ../sentiment.megam.model binary testFileSENTIMENT.txt > sentiment.megam.result

5. Postprocess the result to make the output fit the request:

EX: python3 convertResultFileMegamSENTIMENT.py

6. Calculate F-Score for Negatice  and Positive class: (Only when yuo did about with EX1 in step1&2)

Usage: python3 calFScorePOSMEGAM.py VERIFYMODELFILE TESTFILE

EX: python3 calFScorePOSMEGAM.py verifyModelSENTIMENT.txt ../sentiment.megam.out

Usage: python3 calFScoreNEGMEGAM.py VERIFYMODELFILE TESTFILE

EX: python3 calFScoreNEGMEGAM.py verifyModelSENTIMENT.txt ../sentiment.megam.out
