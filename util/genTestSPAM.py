""" Purpose: Specific for SPAM TEST files extraction. 
	Usage:   python3 genTestSPAM.py dev_file_path, testFileName, verifyModelName(Used this model/correct file to verify and calculate F-Score with real output)
	EX1: python3 genTestSPAM.py '../../SPAM_dev/*.txt' '../testFileSPAM.txt' 'verifyModelSPAM.txt' 
	EX2: python3 genTestSPAM.py '../../SPAM_test/*.txt' '../testFileSPAM.txt' 'verifyModelSPAM.txt' """
import glob
import sys
fileNames = sorted(glob.glob(sys.argv[1]))
testDataName = sys.argv[2]
testModelName = sys.argv[3]

def extractToLines(fileNames, operation):
	testData = open(testDataName, operation)
	testModel = open(testModelName, operation)
	for fileName in fileNames:
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail=""
		for line in lines:
			line = line.lower()
			s = line.strip()
			s += " "
			singleMail += s
		singleMail += '\n'
		if fileName.find("SPAM.")!=-1:
			testModel.write("SPAM\n")
		else:
			testModel.write("HAM\n")
		testData.write(singleMail)
		#print(singleMail)
	testData.close()
def main():
	extractToLines(fileNames, 'w')

if __name__ == '__main__':
	main()
else:
	print("genTestSPAM loaded as a module")
	
