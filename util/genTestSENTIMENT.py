""" Purpose: Specific for SPAM TEST files extraction. 
	Usage:   python3 genTestSENTIMENT.py dev_file test_file verify_file
	EX:  python3 genTestSENTIMENT.py '../../SENTIMENT_dev20/*.txt' '../testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt'
	EX:  python3 genTestSENTIMENT.py '../../SENTIMENT_test/*.txt' '../testFileSENTIMENT.txt' 'verifyModelSENTIMENT.txt' """
import glob
import sys
fileNames = sorted(glob.glob(sys.argv[1]))
testDataName = sys.argv[2]
testModelName = sys.argv[3]

def extractToLines(fileNames, operation):
	testData = open(testDataName, operation)
	testModel = open(testModelName, operation)
	for fileName in fileNames:
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail=""
		for line in lines:
			s = line.strip()
			s += " "
			singleMail += s
		singleMail += '\n'
		if fileName.find("NEG.")!=-1:
			testModel.write("NEG\n")
		else:
			testModel.write("POS\n")
		testData.write(singleMail)
		#print(singleMail)
	testData.close()
def main():
	extractToLines(fileNames, 'w')

if __name__ == '__main__':
	main()
else:
	print("genTestSENTIMENT loaded as a module")
	
