""" Purpose: Specific for NEGative and POSitive files extraction. 
	Usage: python3 genTrainFileSentiment.py fileNames_NEG, fileNames_POS, trainDataName
	EX1: python3 genTrainFileSentiment.py '../../SENTIMENT_training80/NEG.*.txt' '../../SENTIMENT_training80/POS.*.txt' '../sentiment_training.txt' 
	EX2: python3 genTrainFileSentiment.py '../../SENTIMENT_training/NEG.*.txt' '../../SENTIMENT_training/POS.*.txt' '../sentiment_training.txt' """
import glob
import sys
fileNames_NEG = sorted(glob.glob(sys.argv[1]))
fileNames_POS = sorted(glob.glob(sys.argv[2]))
trainDataName = sys.argv[3]

def extractToLines(fileNames, operation, fileType):
	trainData = open(trainDataName, operation)
	for fileName in fileNames:
		#print(fileName)
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail = fileType+ " "
		for line in lines:
			s = line.strip()
			s += " "
			singleMail += s
		singleMail += '\n'
		trainData.write(singleMail)
		#print(singleMail)
	trainData.close()
def main():
	extractToLines(fileNames_NEG, 'w','NEG')
	extractToLines(fileNames_POS, 'a', 'POS')

if __name__ == '__main__':
	main()
	
