""" Purpose: Specific for HAM and SPAM files extraction. 
	Usage: python3 genTrainFileSpam.py train_fileNames_HAM, train_fileNames_SPAM, train_file_path_Data
	EX: python3 genTrainFileSpam.py '../../SPAM_training/HAM.*.txt' '../../SPAM_training/SPAM.*.txt' '../spam_training.txt'"""
import glob
import sys
fileNames_HAM = sorted(glob.glob(sys.argv[1]))
fileNames_SPAM = sorted(glob.glob(sys.argv[2]))
file_path_Data = sys.argv[3]
#fileNames_TEST = glob.glob("../SPAM_training_test/test_*.txt")
#trainDataName = file_path_Data

def extractToLines(trainDataName, fileNames, operation, fileType):
	trainData = open(trainDataName, operation)
	for fileName in fileNames:
		#print(fileName)
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail = fileType+ " "
		for line in lines:
			line = line.lower()
			s = line.strip()
			s += " "
			singleMail += s
		singleMail += '\n'
		trainData.write(singleMail)
		#print(singleMail)
	trainData.close()
def main():
	extractToLines(file_path_Data, fileNames_HAM, 'w+','HAM')
	extractToLines(file_path_Data, fileNames_SPAM, 'a', 'SPAM')
	#extractToLines(fileNames_TEST, 'w','HAM')

if __name__ == '__main__':
	main()
else:
	print("genTrainFileSPAM loaded as a module")
	
