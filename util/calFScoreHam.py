'''Puporse: Calculation F-Score for SPAM filter
	Usage: python3 calFScoreHam.py VERIFYMODELFILE TESTFILE
	EX: python3 calFScoreHam.py verifyModelSPAM.txt ../spam.out'''
import sys
testModelFileName = sys.argv[1]
testFileName = sys.argv[2]


def calPrecision():
	correctlyClassify = 0
	classifyAsC = 0
	list_actual = []
	list_test = []
	in_actual = open(testModelFileName, 'r')
	in_test = open(testFileName,'r')
	lines_actual = in_actual.readlines()
	lines_test = in_test.readlines()
	in_actual.close()
	in_test.close()
	for line in lines_actual:
		words = line.split()
		list_actual.append(words[0])
		#print(words[0])
	for line in lines_test:
		words = line.split()
		list_test.append(words[0])
		#print(words[0])
	itor_actural = iter(list_actual)
	itor_test = iter(list_test)

	try:
		while True:
			word_actual = next(itor_actural)
			word_test = next(itor_test)
			if word_test=='HAM':
				classifyAsC+=1
			if word_actual == 'HAM' and word_test == 'HAM':
				correctlyClassify+=1
	except StopIteration:
		pass
	finally:
		del itor_actural
		del itor_test
	return (correctlyClassify/classifyAsC)


def calRecall():
	correctlyClassify = 0
	belongToC = 0
	list_actual = []
	list_test = []
	in_actual = open(testModelFileName, 'r')
	in_test = open(testFileName,'r')
	lines_actual = in_actual.readlines()
	lines_test = in_test.readlines()
	in_actual.close()
	in_test.close()
	for line in lines_actual:
		words = line.split()
		list_actual.append(words[0])
		#print(words[0])
	for line in lines_test:
		words = line.split()
		list_test.append(words[0])
		#print(words[0])
	itor_actural = iter(list_actual)
	itor_test = iter(list_test)

	try:
		while True:
			word_actual = next(itor_actural)
			word_test = next(itor_test)
			if word_actual=='HAM':
				belongToC+=1
			if word_actual == 'HAM' and word_test == 'HAM':
				correctlyClassify+=1
	except StopIteration:
		pass
	finally:
		del itor_actural
		del itor_test
	return (correctlyClassify/belongToC)




def main():
	print("This is F_score calculation for HAM classify:")
	prescision = calPrecision()
	recall = calRecall()
	fscore = (2*prescision*recall)/(prescision+recall)
	print('prescision = '+str(prescision))
	print('recall = '+str(recall))
	print('F-Score = '+str(fscore))

if __name__ == '__main__':
	main()
else:
	print("nblearn loaded as a module")