""" Usage: python3 nblearn.py TRAININGFILE MODELFILE 
	EX: python3 nblearn.py spam_training.txt spam.nb"""
import sys
import math
trainFileName = sys.argv[1]
modelFileName = sys.argv[2]
classDict = {} #{class1:class1_count, class2:class2_count....}
classTotalWordCountDict = {} #{class1:class1_word_count, class2:class2_word_count}
wordCountDictByClass = {} #{class1:wordCountDict1, class2:wordCountDict2...}
pClasses = {} #{class1: probability_class1, class2: probability_class2}
pWordByClasses = {} ##{class1:wordProbDict1, class2:wordProbDict2...}
wordSize = 0
wordList = []
unknownWordProbByClassDict = {} #{{class1:class1_unknownWordProb, class2:class2_unknownWordProb....}}


#Need to call first
def buildConunts():
	infile = open(trainFileName,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		#line = line.lower()
		words = line.split()
		className = words[0]
		classDict[className] = classDict.get(className, 0) + 1
		wordCountDict = None
		if className in wordCountDictByClass:
			wordCountDict = wordCountDictByClass[className]
		else:
			wordCountDict = {}
			wordCountDictByClass[className]=wordCountDict

		for word in words:
			if word==className or word=='"' or word==',' or word=='.'or word=="'" or word==':' or word==';' or word=='?' or word=='[' or word==']':
				continue
			wordList.append(word)
			wordCountDict[word] = wordCountDict.get(word, 0)+1
			classTotalWordCountDict[className] = classTotalWordCountDict.get(className, 0) + 1

#Need to call after first
def calWordSize():
	#write word size of vocabulary
	wordSet = set(wordList)
	global wordSize
	wordSize = len(wordSet)

#Need to call second
def calProbForClass():
	totalClassCount = 0
	for className in classDict.keys():
		totalClassCount+=classDict[className]
	for className in classDict.keys():
		pClasses[className] = classDict[className]/totalClassCount

#Need to call third
def calProbForWordByClass():
	for className in wordCountDictByClass:
		pWordByClasses[className] = {}
		for word in wordCountDictByClass[className]:
			pWordByClasses[className][word] = (wordCountDictByClass[className][word]+1)/(classTotalWordCountDict[className]+wordSize+1) #using add one!!!

#Need to call before the last & after wordsize is known
def calUnknownProbByClass():
	for className in classTotalWordCountDict:
		unknownWordProbByClassDict[className] = 1/(classTotalWordCountDict[className]+wordSize+1)

#Need to call the last to output result to model file
def writeToOutputFileWithLog10():
	outfile = open(modelFileName, 'w')
	#write p(class)
	for className in pClasses:
		print("pClass "+className+" "+str(math.log10(pClasses[className])), file=outfile)
	#write p(unknown|class) using Add One
	for className in classTotalWordCountDict:
		print("pUnknown "+className+" "+str(math.log10(unknownWordProbByClassDict[className])), file=outfile)

	#write p(w|class)
	for className in pWordByClasses:
		for word in pWordByClasses[className]:
			print(className+" "+word+" "+str(math.log10(pWordByClasses[className][word])), file=outfile)


def main():
	buildConunts()
	'''for key in wordCountDictByClass.keys():
		print(key)
	for className in classDict.keys():
		print(className+": "+str(classDict[className]))
	for className in classTotalWordCountDict.keys():
		print(className+"' total word count: "+str(classTotalWordCountDict[className]))'''
	'''for className in wordCountDictByClass:
		for word in wordCountDictByClass[className]:
			print(className+": "+word+" count = "+str(wordCountDictByClass[className][word]))'''
	calWordSize()
	#print("wordSize="+str(wordSize))
	calProbForClass()
	'''for className in pClasses:
		print(className+"'s probability = "+str(pClasses[className]))'''
	calProbForWordByClass()
	'''for className in pWordByClasses:
		for word in pWordByClasses[className]:
			print(className+": "+word+" probability = "+str(pWordByClasses[className][word]))'''
	calUnknownProbByClass()
	'''for className in classTotalWordCountDict:
		print("pUnknown "+className+" "+str(math.log10(unknownWordProbByClassDict[className])))'''
	writeToOutputFileWithLog10()

if __name__ == '__main__':
	main()
else:
	print("nblearn loaded as a module")



