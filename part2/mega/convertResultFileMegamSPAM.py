import sys
megam_result = 'spam.megam.result'
megam_out = '../spam.megam.out'

def convertResultTo544():
	outfile = open(megam_out, 'w')
	infile = open(megam_result, 'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		result='HAM'
		if float(words[0]) < 0.5:
			result= 'SPAM'
		print(result, file=outfile)
	outfile.close()

def main():
	convertResultTo544()

if __name__ == '__main__':
	main()