import sys
megam_result = 'sentiment.megam.result'
megam_out = '../sentiment.megam.out'

def convertResultTo544():
	outfile = open(megam_out, 'w')
	infile = open(megam_result, 'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		result='POS'
		if float(words[0]) < 0.5:
			result= 'NEG'
		print(result, file=outfile)
	outfile.close()

def main():
	convertResultTo544()

if __name__ == '__main__':
	main()