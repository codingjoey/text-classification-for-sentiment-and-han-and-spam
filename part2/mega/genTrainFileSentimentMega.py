""" Purpose: Specific for HAM and SPAM files extraction using MegaM. 
	Usage: python3 genTrainFileSentimentMega.py train_file_HAM train_file_SPAM train_file
	EX1:  python3 genTrainFileSentimentMega.py '../../../SENTIMENT_training80/POS.*.txt' '../../../SENTIMENT_training80/NEG.*.txt' 'trainDataSENTIMENT_Mega.txt'
	EX2:  python3 genTrainFileSentimentMega.py '../../../SENTIMENT_training/POS.*.txt' '../../../SENTIMENT_training/NEG.*.txt' 'trainDataSENTIMENT_Mega.txt'
	"""
import glob
import sys
fileNames_POS = sorted(glob.glob(sys.argv[1]))
fileNames_NEG = sorted(glob.glob(sys.argv[2]))
file_path_Data = sys.argv[3]
#fileNames_TEST = glob.glob("../SPAM_training_test/test_*.txt")
#trainDataName = file_path_Data

def extractToLines(trainDataName, fileNames, operation, fileType):
	trainData = open(trainDataName, operation)
	for fileName in fileNames:
		#print(fileName)
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail = fileType+ " "
		for line in lines:
			s = line.strip()
			for word in s.split():
				if len(word) <= 1:
					continue
				singleMail += (word + " ")
		singleMail += '\n'
		trainData.write(singleMail)
		#print(singleMail)
	trainData.close()
def main():
	extractToLines(file_path_Data, fileNames_POS, 'w+','1') #1 for POS
	extractToLines(file_path_Data, fileNames_NEG, 'a', '0') # 0 for NEG
	#extractToLines(fileNames_TEST, 'w','HAM')

if __name__ == '__main__':
	main()
	
