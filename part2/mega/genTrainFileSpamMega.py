""" Purpose: Specific for HAM and SPAM files extraction using MegaM. 
	Usage: python3 genTrainFileSpamMega.py train_file_HAM train_file_SPAM train_file
	EX:  python3 genTrainFileSpamMega.py '../../../SPAM_training/HAM.*.txt' '../../../SPAM_training/SPAM.*.txt' 'trainDataSPAM_Mega.txt'
	"""
import glob
import sys
fileNames_HAM = sorted(glob.glob(sys.argv[1]))
fileNames_SPAM = sorted(glob.glob(sys.argv[2]))
file_path_Data = sys.argv[3]
#fileNames_TEST = glob.glob("../SPAM_training_test/test_*.txt")
#trainDataName = file_path_Data

def extractToLines(trainDataName, fileNames, operation, fileType):
	trainData = open(trainDataName, operation)
	for fileName in fileNames:
		#print(fileName)
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		singleMail = fileType+ " "
		for line in lines:
			s = line.strip()
			for word in s.split():
				if len(word) <= 1:
					continue
				singleMail += (word + " ")
		singleMail += '\n'
		trainData.write(singleMail)
		#print(singleMail)
	trainData.close()
def main():
	extractToLines(file_path_Data, fileNames_HAM, 'w+','1') #1 for HAM
	extractToLines(file_path_Data, fileNames_SPAM, 'a', '0') # 0 for SPAM
	#extractToLines(fileNames_TEST, 'w','HAM')

if __name__ == '__main__':
	main()
else:
	print("genTrainFileSPAM loaded as a module")
	
