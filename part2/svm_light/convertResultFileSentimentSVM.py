import sys
svm_result = 'sentiment.svm.result'
svm_out = '../sentiment.svm.out'

def convertResultTo544():
	outfile = open(svm_out, 'w')
	infile = open(svm_result, 'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		words = line.split()
		result='POS'
		if float(words[0]) < 0:
			result= 'NEG'
		print(result, file=outfile)
	outfile.close()

def main():
	convertResultTo544()

if __name__ == '__main__':
	main()