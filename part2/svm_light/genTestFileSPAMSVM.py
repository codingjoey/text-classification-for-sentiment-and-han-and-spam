""" Purpose: Specific for SPAM TEST files extraction using SVM light.
	Usage:   python3 genTestFileSPAMSVM.py dev_file test_file verify_file
	EX1: python3 genTestFileSPAMSVM.py '../../../SPAM_dev/*.txt' 'testFileSPAMSVM.txt' 'verifyModelSPAMSVM.txt'
	EX2: python3 genTestFileSPAMSVM.py '../../../SPAM_test/*.txt' 'testFileSPAMSVM.txt' 'verifyModelSPAMSVM.txt' """
import glob
import sys
fileNames = sorted(glob.glob(sys.argv[1]))
testDataName = sys.argv[2]
testModelName = sys.argv[3]
wordIdTableFile = "wordIdTableSPAM.txt"
wordIdDict = {} #{word:Id}

def buildWordIdTable():
	global wordIdDict
	infile = open(wordIdTableFile, 'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		s = line.strip()
		words = s.split()
		wordIdDict[words[1]]=words[0]

def extractToLines(fileNames, operation):
	testData = open(testDataName, operation)
	testModel = open(testModelName, operation)
	for fileName in fileNames:
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		fileType=1
		if fileName.find("SPAM.")!=-1:
			testModel.write("SPAM\n")
			fileType=-1
		else:
			testModel.write("HAM\n")

		wordIdCountDict = {} # {word ID : word count in this document}
		singleMail = str(fileType)+ " "
		for line in lines:		
			s = line.strip()
			words = s.split()
			for word in words:
				#print(word)
				if word not in wordIdDict:
					wordIdCountDict[1] = wordIdCountDict.get(1, 0) + 1
					continue
				#print(wordIdDict.get(word))
				wordIdCountDict[wordIdDict.get(word)] = wordIdCountDict.get(wordIdDict.get(word), 0) + 1
		#for x in wordIdCountDict:
		#	print(str(x)+":"+str(wordIdCountDict.get(x)))
		sorted_list = [x for x in wordIdCountDict.items()] 
		sorted_list.sort(key=lambda x: int(x[0]))
		for x in sorted_list:
			#print(str(x[0])+" "+str(x[1]))	
			singleMail += str(x[0])
			singleMail += ":"
			singleMail += str(x[1])
			singleMail += " "
		singleMail += '\n'
		testData.write(singleMail)
		#print(singleMail)
	testData.close()
	testModel.close()


def main():
	buildWordIdTable()
	'''for key in wordIdDict:
		print(str(key)+":"+str(wordIdDict.get(key)))'''
	extractToLines(fileNames, 'w')

if __name__ == '__main__':
	main()