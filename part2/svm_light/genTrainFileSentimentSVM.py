""" Purpose: Specific for HAM and SPAM files extraction using SVM light
	Usage: python3 genTrainFileSentimentSVM.py train_file_POS train_file_NEG train_file_name
	EX1:  python3 genTrainFileSentimentSVM.py '../../../SENTIMENT_training80/POS.*.txt' '../../../SENTIMENT_training80/NEG.*.txt' 'trainDataSENTIMENT_SVM.txt'
	EX2:  python3 genTrainFileSentimentSVM.py '../../../SENTIMENT_training/POS.*.txt' '../../../SENTIMENT_training/NEG.*.txt' 'trainDataSENTIMENT_SVM.txt' """
import glob
import sys
fileNames_POS = sorted(glob.glob(sys.argv[1]))
fileNames_NEG = sorted(glob.glob(sys.argv[2]))
file_path_Data = sys.argv[3]
fileName_wordIdTable = 'wordIdTableSENTIMENT.txt'
wordIdDict = {} #{word:Id}
wordId = 2 # assign Id number for word, 1 for unknown word

def extractToLines(trainDataName, fileNames, operation, fileType):
	global wordIdDict
	global wordId
	trainData = open(trainDataName, operation)
	for fileName in fileNames:
		#print(fileName)
		infile = open(fileName,'r', errors='ignore')
		lines = infile.readlines()
		infile.close()
		wordIdCountDict = {} # {word ID : word count in this document}
		singleMail = fileType+ " "
		for line in lines:		
			s = line.strip()
			words = s.split()
			for word in words:
				if word not in wordIdDict:
					wordIdDict[word] = wordId
					wordId+=1
				wordIdCountDict[wordIdDict.get(word)] = wordIdCountDict.get(wordIdDict.get(word), 0) + 1
		sorted_list = [x for x in wordIdCountDict.items()] 
		sorted_list.sort(key=lambda x: x[0])
		for x in sorted_list:
			#print(str(x[0])+" "+str(x[1]))	
			singleMail += str(x[0])
			singleMail += ":"
			singleMail += str(x[1])
			singleMail += " "
		singleMail += '\n'
		trainData.write(singleMail)
			#print(singleMail)
	trainData.close()

def printWordIdToFiles():
	global wordIdDict
	global fileName_wordIdTable
	outfile = open(fileName_wordIdTable, 'w')
	sorted_list = [x for x in wordIdDict.items()] 
	sorted_list.sort(key=lambda x: x[1])
	for x in sorted_list:
		#outfile.write(str(x))
		print(str(x[1])+' '+str(x[0]),file=outfile)
		#print(x)


def main():
	extractToLines(file_path_Data, fileNames_POS, 'w+','1')# 1 for POSitive
	extractToLines(file_path_Data, fileNames_NEG, 'a', '-1')# -1 for NEGative
	printWordIdToFiles()
	#extractToLines(fileNames_TEST, 'w','HAM')

if __name__ == '__main__':
	main()