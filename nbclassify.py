''' Usage: python3 nbclassify.py MODELFILE TESTFILE
	EX: python3 nbclassify.py spam.nb testFileSPAM.txt > spam.out'''
import sys
modelFileName = sys.argv[1]
testFileName = sys.argv[2]
#outFile = "spam.out"
#outFile = "sentiment.out"
pClasses = {} #{class1: probability_class1, class2: probability_class2}
pWordByClasses = {} ##{class1:wordProbDict1, class2:wordProbDict2...}
unknownWordProbByClassDict = {} #{{class1:class1_unknownWordProb, class2:class2_unknownWordProb....}}
probabilityOfClassesIndicator = 'pClass'
probabilityOfUnknownWordByClassIndicator = 'pUnknown'
classifyResultList = []

# Need to call first
def readModelFile():
	infile = open(modelFileName,'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		#line = line.lower()
		words = line.split()
		if words[0]==probabilityOfClassesIndicator:
			pClasses[words[1]] = words[2]
		elif words[0]==probabilityOfUnknownWordByClassIndicator:
			unknownWordProbByClassDict[words[1]] = words[2]
		else:
			className = words[0]
			word = words[1]
			probabilityLog = words[2]
			pWordByClassDict = None
			if className in pWordByClasses:
				pWordByClassDict = pWordByClasses[className]
			else:
				pWordByClassDict = {}
				pWordByClasses[className] = pWordByClassDict
			pWordByClassDict[word] = probabilityLog #p(word|class) (log10)

# Need to call after first
def classify():
	finalProbByClass = {} #{ P(class1|w), P(class2|w), P(class3|w),.....}
	infile = open(testFileName, 'r')
	lines = infile.readlines()
	infile.close()
	for line in lines:
		#line = line.lower()
		words = line.split()
		for className in pClasses:
			#pMsgInClass=0.0
			nominator = float(pClasses[className])
			#print(className+":"+nominator)
			for word in words:
				if word in pWordByClasses[className]:
					nominator += float(pWordByClasses[className][word])
				else:
					nominator += float(unknownWordProbByClassDict[className])
			'''denominator = nominator
			for word in words:
				for classNamedd in pClasses:
					if classNamedd == className:
						continue
					if word in pWordByClasses[classNamedd]:	
						denominator += float(pWordByClasses[classNamedd][word])
					else:
						denominator += float(unknownWordProbByClassDict[classNamedd])
			pMsgInClass = nominator - denominator
			finalProbByClass[className] = pMsgInClass'''
			finalProbByClass[className] = nominator
		maxProb = max(finalProbByClass.values())
		finalClassList = [x for x,y in finalProbByClass.items() if y == maxProb]
		classifyResultList.append(finalClassList[0])




def main():
	readModelFile()
	'''for className in pClasses:
		print(className+"'s probability = "+str(pClasses[className]))
	for className in pClasses:
		print(className+"'s unknow word probability = "+str(unknownWordProbByClassDict[className]))'''
	classify()
	#outF = open(outFile, 'w')
	for result in classifyResultList:
		print(result)
		#print(result, file=outF)
	#outF.close()

if __name__ == '__main__':
	main()
else:
	print("nblearn loaded as a module")